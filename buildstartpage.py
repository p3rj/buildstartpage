######################################################################
#
# buildstartpage.py -- A Python script to generate a browser start
# page from a list of links.
#
# Input file format:
#
# [Section name]
# url;title;alt_title
# ...
#
# Links are grouped using names in brackets []. Each link consists of
# a url optionally followed by the title representing the link. If
# the title is long or otherwise inappropriate, an alternative title
# can be added. This is then used as the representation, and the
# original title becomes a tooltip. The parts of the link need to be
# separated by semicolon ; characters. So a link may look like
#
# https://www.reddit.com/r/suggestapc/;When Building it Yourself Isn't an Option;/r/suggestapc
#
######################################################################
#
# Copyright 2018 Peter Jahn
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
######################################################################

import argparse
import collections
import csv
import sys

class SectionReadingFileWrapper:
    """Wrap a file, so reading it extracts sections as a side-effect.
    
    When iterated over, line by line is returned from the file given
    to the constructor, with the following extra processing:
    - all leading and trailing white space is trimmed initially;
    - section headers [section name] aren't returned, instead the
      name is extracted and made available as the section field;
    - empty lines are skipped over;
    - comments (lines beginning with # or ;) are skipped over as
      well;
    - the extra string given to the constructor is appended to each
      line returned.
    The latter is useful in combination with csv.reader if the result
    can contain a certain number of optional fields (with possibly
    empty default values). One can then add the defaults to each line,
    so the fields may be used without further testing (obviously there
    will be excess fields for input lines already containing all
    fields).
    The wrapped file must implement readline.
    """
    
    def __init__(self, source, extra=''):
        """Return a reader wrapping the given file.
        
        The given extra string is appended to each line returned.
        See the class description for more information.
        """
        self.source = source
        self.extra = extra
        self.section = 'Uncategorized'
    
    def __iter__(self):
        """Return the iterator object."""
        return self
    
    def __next__(self):
        """Return the next line from the wrapped file.
        
        See the class description for more information.
        """
        while True:
            line = self.source.readline()
            if line == '':
                raise StopIteration
            line = line.strip()
            if len(line) > 0:
                if line[0] == '[':
                    if line[-1] == ']':
                        self.section = line[1:-1]
                elif line[0] != '#' and line[0] != ';':
                    return f'{line}{self.extra}'


class SectionsDict(collections.OrderedDict):
    """An ordered dictionary with entries initialized as empty lists.
    
    Used for holding the sections read from the input. Sections names
    serve as the keys here. Sections should be kept in the order they
    appear in the input, this is supported by iterating the ordered
    dictionary. For easy handling, each undefined key yields an empty
    list.
    """
    
    def __missing__(self, key):
        """Initialize non-existant entry with empty list."""
        self[key] = []
        return self[key]


def load(f):
    """Load the links from the given file."""
    d = SectionReadingFileWrapper(f, extra=';;')
    r = csv.reader(d, delimiter=';')
    for link in r:
        sections[d.section].append({'url': link[0], 'title': link[1], 'alt_title': link[2]})


arg_parser = argparse.ArgumentParser(description='Create a browser start page')
arg_parser.add_argument('input_files', metavar='filename', type=argparse.FileType('r'), nargs='*')
args = arg_parser.parse_args()

sections = SectionsDict()
if len(args.input_files) > 0:
    for f in args.input_files:
        load(f)
else:
    load(sys.stdin)

for s in sections:
    print(f'{s}: {sections[s]}')
